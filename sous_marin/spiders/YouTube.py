import scrapy
import re
import urllib.parse

class YouTubeSpider(scrapy.Spider):
    name = 'youtube'
    allowed_domains = ['youtube.com']
    start_urls = ['https://www.youtube.com/c/NBCNews/about',
                  'https://www.youtube.com/naofazsentido/about',
                  'https://www.youtube.com/c/tvsenado/about',
                  'https://www.youtube.com/c/Ou%C3%A7aTV/about',
                  'https://www.youtube.com/c/2Carneiros/about',
                  'https://www.youtube.com/c/OfficialABBA/about',
                  'https://www.youtube.com/c/yaxu/about',
                  'https://www.youtube.com/c/AlgoMech/about',
                  'https://www.youtube.com/channel/UCvbhUpBqfHmyDG5NqTgIEjA/about',
                  'https://www.youtube.com/c/AlphaBlondyOff/about',
                  'https://www.youtube.com/c/arteplus7fr/about',
                  'https://www.youtube.com/channel/UCoWG3EwV1IaOZsbrIbLPVfQ/about']

    def parse(self, response):
        #<form action="https://consent.youtube.com/s" method="POST" style="display:inline;">
        #  <input type="hidden" name="gl" value="FR">
        #  <input type="hidden" name="m" value="0">
        #  <input type="hidden" name="pc" value="yt">
        #  <input type="hidden" name="continue" value="https://www.youtube.com/c/NBCNews/about">
        #  <input type="hidden" name="ca" value="r">
        #  <input type="hidden" name="x" value="8">
        #  <input type="hidden" name="v" value="cb.20211026-09-p1.en+FX+402">
        #  <input type="hidden" name="t" value="ADw3F8jcggDPBE2Zb6ull4Dy0wMVqMXOwg:1635461878205">
        #  <input type="hidden" name="hl" value="en">
        #  <input type="hidden" name="src" value="1">
        #  <input type="hidden" name="uxe" value="23983171">
        #  <input type="submit" value="I agree" class="button" aria-label="Agree to the use of cookies and other data for the purposes described"/>
        #</form>
        return scrapy.FormRequest.from_response(
          response,
          callback=self.cookie_accepted
        )

    def cookie_accepted(self, response):
        channelId = response.css('meta[itemprop="channelId"]').attrib['content']
        body = response.body.decode("utf-8")
        links = {}
        for m in re.finditer('"(https://www.youtube.com/redirect[^"]+)"', body):
          link = urllib.parse.unquote(m.group(0).split('q=')[1]).replace('\"', '')
          links[link] = 1
        for link in links:
          yield { 'channelId': channelId, 'link': link }
        pass
