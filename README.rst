================
sous-marin rouge
================

------------
Installation
------------

You need to have Python and Poetry, see Poetry docs on how to install:

* https://python-poetry.org/docs/#installation

Once you have Poetry installed then run can install the ``sous-marin rouge``
dependencies:

.. code-block:: shell

   poetry install

---
Run
---

Run the crawler, the outputs will be saved on the JSON file ``youtube.json``:

.. code-block:: shell

   poetry run scrapy crawl youtube -O youtube.json

To output CSV instead of JSON just run:

.. code-block:: shell

   poetry run scrapy crawl youtube -O youtube.csv


-------
Authors
-------

* Joenio M. Costa
* Kurt Maxwell Kusterer
